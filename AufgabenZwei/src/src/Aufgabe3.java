package src;


public final class Aufgabe3 
{ public static void main( final java.lang.String[] args )
	  

{ 		
		java.lang.System.out.printf("%-12s| %10s%n" , "Fahrenheit" , "Celsius");
		java.lang.System.out.printf("%22s%n" , "--------------------" );
		java.lang.System.out.printf("%+-12d| %10.2f %n" , -20 , -28.8889);
		java.lang.System.out.printf("%+-12d| %10.2f %n" , -10 , -23.33);
		java.lang.System.out.printf("%+-12d| %10.2f %n" , +0 , -17.78);
		java.lang.System.out.printf("%+-12d| %10.2f %n" , +20 , -6.67);
		java.lang.System.out.printf("%+-12d| %10.2f %n" , +30 , -1.11);
	
}
}