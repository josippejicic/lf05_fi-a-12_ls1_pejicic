
public class Huelle {

	private int prozent;

	
	public Huelle(int prozent) {
		
		this.prozent = prozent;
	}

	public int getProzent() {
		return prozent;
	}

	public void setProzent (int prozent) {
		this.prozent = prozent;
	}

}
